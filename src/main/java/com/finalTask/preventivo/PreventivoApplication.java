package com.finalTask.preventivo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PreventivoApplication {

	public static void main(String[] args) {
		SpringApplication.run(PreventivoApplication.class, args);
	}

}
