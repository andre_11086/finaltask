package com.finalTask.preventivo.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.finalTask.preventivo.models.Indirizzo;
import com.finalTask.preventivo.services.IndirizzoServices;

@RestController
@RequestMapping("/indirizzo")
@CrossOrigin(origins="*", allowedHeaders="*")
public class IndirizzoController {
	
	@Autowired
	private IndirizzoServices services;

	@GetMapping("/test")
	public String test() {
		return "test works";
	}
	
	@PostMapping("/insert")
	public Indirizzo insert(@RequestBody Indirizzo ind) {
		return services.save(ind);
	}
	
	@GetMapping("/findOne")
	public Indirizzo findById(int id) {
		return services.findById(id);
	}
	
	@GetMapping("/findAll")
	public List<Indirizzo> findAll(){
		return services.findAll();
	}
	
	@DeleteMapping("/delete")
	public boolean delete(int id) {
		return services.delete(id);
	}
	
	@PutMapping("/update")
	public boolean update(@RequestBody Indirizzo ind) {
		return services.update(ind);
	}
	
}
