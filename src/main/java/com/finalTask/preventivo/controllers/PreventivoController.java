package com.finalTask.preventivo.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.finalTask.preventivo.models.Preventivo;
import com.finalTask.preventivo.models.Prodotto;
import com.finalTask.preventivo.services.PreventivoServices;
import com.finalTask.preventivo.services.ProdottoServices;

@RestController
@RequestMapping("/preventivo")
@CrossOrigin(origins="*", allowedHeaders="*")
public class PreventivoController {
	
	@Autowired
	private PreventivoServices services;
	@Autowired
	private ProdottoServices prodServ;

	@GetMapping("/test")
	public String test() {
		return "test works";
	}
	
	@PostMapping("/insert")
	public Preventivo insert(@RequestBody Preventivo prev) {
		return services.save(prev);
	}
	
	@GetMapping("/findOne")
	public Preventivo findOne(int id) {
		return services.findById(id);
	}
	
	@GetMapping("/findAll")
	public List<Preventivo> findAll(){
		return services.findAll();
	}
	
	@DeleteMapping("/delete")
	public boolean delete(int id) {
		return services.delete(id);
	}
	
	@PutMapping("/update")
	public boolean update( @RequestBody Preventivo prev) {
		return services.update(prev);
	}
	
	@GetMapping("/includi")
	public Preventivo includi(int id_preventivo, @RequestBody List<Prodotto> products) {
		Preventivo temp = services.findById(id_preventivo);
		for (Prodotto prodotto : products) {
			temp.getProdotti().add(prodServ.findById(prodotto.getId_prodotto()));
		}
		this.services.update(temp);
		return temp;
	}
	
}
