package com.finalTask.preventivo.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.finalTask.preventivo.models.Categoria;
import com.finalTask.preventivo.services.CategoriaServices;

@RestController
@RequestMapping("/categoria")
@CrossOrigin(origins="*", allowedHeaders="*")
public class CategoriaController {
	
	@Autowired
	private CategoriaServices services;

	@GetMapping("/test")
	public String test() {
		return "test works";
	}
	
	@PostMapping("/insert")
	public Categoria insert(@RequestBody Categoria cat) {
		return services.save(cat);
	}
	
	@GetMapping("/findOne")
	public Categoria findById(int id) {
		return services.findById(id);
	}
	
	@GetMapping("/findAll")
	public List<Categoria> findAll(){
		return services.findAll();
	}
	
	@DeleteMapping("/delete")
	public boolean delete(int id) {
		return services.delete(id);
	}
	
	@PutMapping("/update")
	public boolean update(@RequestBody Categoria cat) {
		return services.update(cat);
	}
}
