package com.finalTask.preventivo.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.finalTask.preventivo.models.Azienda;
import com.finalTask.preventivo.services.AziendaServices;
import com.finalTask.preventivo.services.IndirizzoServices;

@RestController
@RequestMapping("/azienda")
@CrossOrigin(origins="*", allowedHeaders="*")
public class AziendaController {

	@Autowired
	private AziendaServices services;
	
	
	@GetMapping("/test")
	public String test() {
		return "test works";
	}
	
	@PostMapping("/insert")
	public Azienda insert(@RequestBody Azienda az) {
		return services.save(az);
	}
	
	@GetMapping("/findOne")
	public Azienda findOne(int id) {
		return services.findById(id);
	}
	
	@GetMapping("/findAll")
	public List<Azienda> findAll(){
		return services.findAll();
	}
	
	@DeleteMapping("/delete")
	public boolean delete(int id) {
		return services.delete(id);
	}
	
	@PutMapping("/update")
	public boolean update(@RequestBody Azienda az) {
		return services.update(az);
	}
}
