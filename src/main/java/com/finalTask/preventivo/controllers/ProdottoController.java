package com.finalTask.preventivo.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.finalTask.preventivo.models.Categoria;
import com.finalTask.preventivo.models.Prodotto;
import com.finalTask.preventivo.services.CategoriaServices;
import com.finalTask.preventivo.services.ProdottoServices;

@RestController
@RequestMapping("/prodotto")
@CrossOrigin(origins="*", allowedHeaders="*")
public class ProdottoController {
	
	@Autowired
	private ProdottoServices services;
	
	@Autowired
	private CategoriaServices catServices;
	
	@GetMapping("/test")
	public String test() {
		return "test works";
	}

	@PostMapping("/insert")
	public Prodotto insert(@RequestBody Prodotto prod) {
		return services.save(prod);
	}
	
	@GetMapping("/findOne")
	public Prodotto findById(int id) {
		return services.findById(id);
	}
	

	@GetMapping("/findAll")
	public List<Prodotto> findAll() {
		return services.findAll();
	}
	
	@DeleteMapping("/delete")
	public boolean delete(int id) {
		return services.delete(id);
	}
	
	@PutMapping("/update")
	public boolean update(@RequestBody Prodotto prod) {
		return services.update(prod);
	}
	
	@GetMapping("/associa")
	public Prodotto associaConCategoria(int id_prodotto, @RequestBody ArrayList<Categoria> id_categoria) {
		Prodotto temp = services.findById(id_prodotto);
		for (Categoria categoria : id_categoria) {
			temp.getCategorie().add(catServices.findById(categoria.getId_categoria()));
		}
		services.update(temp);
		return temp;
	}
	
}
