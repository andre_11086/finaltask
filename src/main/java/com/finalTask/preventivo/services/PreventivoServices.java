package com.finalTask.preventivo.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.finalTask.preventivo.models.Preventivo;
import com.finalTask.preventivo.repositories.DataAccessRepo;
@Service
public class PreventivoServices implements DataAccessRepo<Preventivo>{

	@Autowired
	private EntityManager entMan;

	private Session getSession() {
		return entMan.unwrap(Session.class);
	}

	@Override
	public Preventivo save(Preventivo t) {
		Preventivo prev = new Preventivo();
		prev.setCliente(t.getCliente());
		//prev.setData_preventivo(t.getData_preventivo());
		prev.setNumero_preventivo(t.getNumero_preventivo());
		prev.setPrezzo_totale(t.getPrezzo_totale());
		prev.setProdotti(t.getProdotti());
		prev.setVoci(t.getVoci());
		
		try {
			getSession().save(prev);
			return prev;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	@Override
	public Preventivo findById(int id) {
		return (Preventivo) getSession().createCriteria(Preventivo.class)
				.add(Restrictions.eqOrIsNull("id_preventivo", id)).uniqueResult();
	}

	@Override
	public List<Preventivo> findAll() {
		return getSession().createCriteria(Preventivo.class).list();
	}

	@Transactional
	@Override
	public boolean delete(int id) {
		try {
			getSession().delete(getSession().load(Preventivo.class, id));
			getSession().flush();
			return true;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return false;
	}

	@Transactional
	@Override
	public boolean update(Preventivo t) {
		try {
			Preventivo temp = getSession().load(Preventivo.class, t.getId_preventivo());
			if(temp != null) {
				temp.setCliente(t.getCliente());
				temp.setData_preventivo(t.getData_preventivo());
				temp.setNumero_preventivo(t.getNumero_preventivo());
				temp.setPrezzo_totale(t.getPrezzo_totale());
				temp.setProdotti(t.getProdotti());
				temp.setVoci(t.getVoci());
				
				getSession().update(temp);
				getSession().flush();
				return true;
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return false;
	}
}
