package com.finalTask.preventivo.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.finalTask.preventivo.models.Prodotto;
import com.finalTask.preventivo.models.Categoria;
import com.finalTask.preventivo.repositories.DataAccessRepo;

@Service
public class ProdottoServices implements DataAccessRepo<Prodotto> {

	@Autowired
	private EntityManager entMan;

	private Session getSession() {
		return entMan.unwrap(Session.class);
	}
	
	@Autowired
	private CategoriaServices categoriaService;

	@Override
	public Prodotto save(Prodotto t) {
		System.out.println("cat " + t.getCategorie());
		Prodotto temp = new Prodotto();
		temp.setCategorie(t.getCategorie());
		temp.setCodice(t.getCodice());
		temp.setNome(t.getNome());
		temp.setPreventivi(t.getPreventivi());
		temp.setPrezzo(t.getPrezzo());
		temp.setQuantita(t.getQuantita());

		try {
			getSession().save(temp); 
			return temp;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	@Override
	public Prodotto findById(int id) {
		return (Prodotto) getSession().createCriteria(Prodotto.class).add(Restrictions.eqOrIsNull("id_prodotto", id))
				.uniqueResult();
	}

	@Override
	public List<Prodotto> findAll() {
		return getSession().createCriteria(Prodotto.class).list();
	}

	@Transactional
	@Override
	public boolean delete(int id) {
		try {
			getSession().delete(getSession().load(Prodotto.class, id));
			getSession().flush();
			return true;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		return false;
	}

	@Transactional
	@Override
	public boolean update(Prodotto t) {
		try {
			Prodotto temp = getSession().load(Prodotto.class, t.getId_prodotto());
			if (temp != null) {
				temp.setCategorie(t.getCategorie());
				temp.setCodice(t.getCodice());
				temp.setNome(t.getNome());
				temp.setPreventivi(t.getPreventivi());
				temp.setPrezzo(t.getPrezzo());
				temp.setQuantita(t.getQuantita());

				getSession().update(temp);
				getSession().flush();
				return true;
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return false;
	}

}
