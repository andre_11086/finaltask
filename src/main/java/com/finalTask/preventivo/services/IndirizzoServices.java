package com.finalTask.preventivo.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.finalTask.preventivo.models.Indirizzo;
import com.finalTask.preventivo.repositories.DataAccessRepo;
@Service
public class IndirizzoServices implements DataAccessRepo<Indirizzo>{
	@Autowired
	private EntityManager entMan;

	private Session getSession() {
		return entMan.unwrap(Session.class);
	}

	@Override
	public Indirizzo save(Indirizzo t) {
		Indirizzo temp = new Indirizzo();
		temp.setVia(t.getVia());
		temp.setCitta(t.getCitta());
		temp.setCap(t.getCap());
		temp.setProvincia(t.getProvincia());
		
		try {
			getSession().save(temp);
			return temp;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		return null;
	}

	@Override
	public Indirizzo findById(int id) {
		return (Indirizzo) getSession().createCriteria(Indirizzo.class)
				.add(Restrictions.eqOrIsNull("id_indirizzo", id))
				.uniqueResult();
	}

	@Override
	public List<Indirizzo> findAll() {
		return getSession().createCriteria(Indirizzo.class).list();
	}
	
	@Transactional
	@Override
	public boolean delete(int id) {
		
		try {
			getSession().delete(getSession().load(Indirizzo.class, id));
			getSession().flush();
			return true;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		return false;
	}
	@Transactional
	@Override
	public boolean update(Indirizzo t) {
		try {
			Indirizzo temp = getSession().load(Indirizzo.class, t.getId_indirizzo());
			if(temp != null) {
				temp.setId_indirizzo(t.getId_indirizzo());
				temp.setVia(t.getVia());
				temp.setCitta(t.getCitta());
				temp.setCap(t.getCap());
				temp.setProvincia(t.getProvincia());
				
				getSession().update(temp);
				getSession().flush();
				return true;
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		return false;
	}
}
