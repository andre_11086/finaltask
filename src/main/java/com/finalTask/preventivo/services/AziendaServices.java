package com.finalTask.preventivo.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import com.finalTask.preventivo.models.Azienda;
import com.finalTask.preventivo.models.Prodotto;
import com.finalTask.preventivo.repositories.DataAccessRepo;
@Service
public class AziendaServices implements DataAccessRepo<Azienda>{
	@Autowired
	private EntityManager entMan;

	@Autowired
	private IndirizzoServices indirizzo;
	
	private Session getSession() {
		return entMan.unwrap(Session.class);
	}

	@Override
	public Azienda save(Azienda t) {
		
		Azienda temp = new Azienda();
		temp.setNome(t.getNome());
		temp.setIndirizzo(indirizzo.findById(t.getIndirizzo().getId_indirizzo()));
		
		try {
			getSession().save(temp);
			return temp;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	@Override
	public Azienda findById(int id) {
		return (Azienda) getSession().createCriteria(Azienda.class)
				.add(Restrictions.eqOrIsNull("id_azienda", id))
				.uniqueResult();
	}

	@Override
	public List<Azienda> findAll() {
		return getSession().createCriteria(Azienda.class).list();
	}
	
	@Transactional
	@Override
	public boolean delete(int id) {
		try {
			getSession().delete(getSession().load(Azienda.class, id));
			getSession().flush();
			return true;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return false;
	}
	@Transactional
	@Override
	public boolean update(Azienda t) {
		try {
			Azienda temp = getSession().load(Azienda.class, t.getId_azienda());
			if(temp != null) {
				temp.setIndirizzo(t.getIndirizzo());
				temp.setNome(t.getNome());
				temp.setId_azienda(t.getId_azienda());
				
				getSession().update(temp);
				getSession().flush();
				return true;
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return false;
	}
}
