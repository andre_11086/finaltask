package com.finalTask.preventivo.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.finalTask.preventivo.models.Categoria;
import com.finalTask.preventivo.repositories.DataAccessRepo;
@Service
public class CategoriaServices implements DataAccessRepo<Categoria>{
	@Autowired
	private EntityManager entMan;

	private Session getSession() {
		return entMan.unwrap(Session.class);
	}

	@Override
	public Categoria save(Categoria t) {
		Categoria cat = new Categoria();
		cat.setNome(t.getNome());
		
		try {
			getSession().save(cat);
			return cat;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	@Override
	public Categoria findById(int id) {
		return (Categoria) getSession().createCriteria(Categoria.class)
				.add(Restrictions.eqOrIsNull("id_categoria", id))
				.uniqueResult();
	}

	@Override
	public List<Categoria> findAll() {
		return getSession().createCriteria(Categoria.class).list();
	}
	@Transactional
	@Override
	public boolean delete(int id) {
		try {
			getSession().delete(getSession().load(Categoria.class, id));
			getSession().flush();
			return true;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return false;
	}
	@Transactional
	@Override
	public boolean update(Categoria t) {
		try {
			Categoria cat = getSession().load(Categoria.class, t.getId_categoria());
			if(cat != null) {
				cat.setNome(t.getNome());
				
				getSession().update(cat);
				getSession().flush();
				return true;
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return false;
	}
}
