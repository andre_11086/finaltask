package com.finalTask.preventivo.models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="azienda")
public class Azienda {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private int id_azienda;
	@Column
	private String nome;
	
	@ManyToOne
	@JoinColumn(name="rif_indirizzo")
	private Indirizzo indirizzo;
	
	@JsonManagedReference
	@OneToMany(mappedBy="cliente")
	private List<Preventivo> preventivi;
	
	
	public int getId_azienda() {
		return id_azienda;
	}
	public void setId_azienda(int id_azienda) {
		this.id_azienda = id_azienda;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Indirizzo getIndirizzo() {
		return indirizzo;
	}
	public void setIndirizzo(Indirizzo indirizzo) {
		this.indirizzo = indirizzo;
	}
	public List<Preventivo> getPreventivi() {
		return preventivi;
	}
	public void setPreventivi(List<Preventivo> preventivi) {
		this.preventivi = preventivi;
	}
	
	
}
