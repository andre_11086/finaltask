package com.finalTask.preventivo.models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="indirizzo")
public class Indirizzo {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private int id_indirizzo;
	@Column
	private String via;
	@Column
	private String citta;
	@Column
	private int cap;
	@Column
	private String provincia;
	
	@OneToMany(mappedBy="indirizzo")
	private List<Azienda> azienda;
	
	public int getId_indirizzo() {
		return id_indirizzo;
	}
	public void setId_indirizzo(int id_indirizzo) {
		this.id_indirizzo = id_indirizzo;
	}
	public String getVia() {
		return via;
	}
	public void setVia(String via) {
		this.via = via;
	}
	public String getCitta() {
		return citta;
	}
	public void setCitta(String citta) {
		this.citta = citta;
	}
	public int getCap() {
		return cap;
	}
	public void setCap(int cap) {
		this.cap = cap;
	}
	public String getProvincia() {
		return provincia;
	}
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	@Override
	public String toString() {
		return "Indirizzo [id_indirizzo=" + id_indirizzo + ", via=" + via + ", citta=" + citta + ", cap=" + cap
				+ ", provincia=" + provincia + ", azienda=" + azienda + "]";
	}
	
	
}
