package com.finalTask.preventivo.models;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="preventivo")
public class Preventivo {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private int id_preventivo;
	
	@ManyToOne
	@JoinColumn(name="rif_azienda")
	@JsonBackReference
	private Azienda cliente;
	
	@Column
	private String numero_preventivo;
	
	@ManyToMany
	@JoinTable(
			name= "preventivo_azienda_prodotto",
			joinColumns= {@JoinColumn(name="rif_preventivo", referencedColumnName="id_preventivo")},
			inverseJoinColumns = {@JoinColumn(name="rif_prodotto", referencedColumnName = "id_prodotto")}
	)
	private List<Prodotto> prodotti;
	@Column
	private String voci;
	@Column(insertable = false)
	private String data_preventivo;
	@Column
	private float prezzo_totale;
	
	public int getId_preventivo() {
		return id_preventivo;
	}
	public void setId_preventivo(int id_preventivo) {
		this.id_preventivo = id_preventivo;
	}
	public Azienda getCliente() {
		return cliente;
	}
	public void setCliente(Azienda cliente) {
		this.cliente = cliente;
	}
	public List<Prodotto> getProdotti() {
		return prodotti;
	}
	public void setProdotti(List<Prodotto> prodotti) {
		this.prodotti = prodotti;
	}
	public String getVoci() {
		return voci;
	}
	public void setVoci(String voci) {
		this.voci = voci;
	}
	public String getData_preventivo() {
		return data_preventivo;
	}
	public void setData_preventivo(String data_preventivo) {
		this.data_preventivo = data_preventivo;
	}
	public float getPrezzo_totale() {
		return prezzo_totale;
	}
	public void setPrezzo_totale(float prezzo_totale) {
		this.prezzo_totale = prezzo_totale;
	}
	public String getNumero_preventivo() {
		return numero_preventivo;
	}
	public void setNumero_preventivo(String numero_preventivo) {
		this.numero_preventivo = numero_preventivo;
	}
	
}
