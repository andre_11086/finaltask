package com.finalTask.preventivo.models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name="prodotto")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id_prodotto")
public class Prodotto {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private int id_prodotto;
	@Column
	private String codice;
	@Column
	private String nome;
	@Column
	private int quantita;
	@Column
	private float prezzo;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(
			name= "prodotto_categoria",
			joinColumns= {@JoinColumn(name="rif_prodotto", referencedColumnName="id_prodotto")},
			inverseJoinColumns = {@JoinColumn(name="rif_categoria", referencedColumnName = "id_categoria")}
	)
	private List<Categoria> categorie;
	
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(
			name= "preventivo_azienda_prodotto",
			joinColumns= {@JoinColumn(name="rif_prodotto", referencedColumnName="id_prodotto")},
			inverseJoinColumns = {@JoinColumn(name="rif_preventivo", referencedColumnName = "id_preventivo")}
	)
	private List<Preventivo> preventivi;

	public int getId_prodotto() {
		return id_prodotto;
	}

	public void setId_prodotto(int id_prodotto) {
		this.id_prodotto = id_prodotto;
	}

	public String getCodice() {
		return codice;
	}

	public void setCodice(String codice) {
		this.codice = codice;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getQuantita() {
		return quantita;
	}

	public void setQuantita(int quantita) {
		this.quantita = quantita;
	}

	public float getPrezzo() {
		return prezzo;
	}

	public void setPrezzo(float prezzo) {
		this.prezzo = prezzo;
	}

	public List<Categoria> getCategorie() {
		return categorie;
	}

	public void setCategorie(List<Categoria> categorie) {
		this.categorie = categorie;
	}

	public List<Preventivo> getPreventivi() {
		return preventivi;
	}

	public void setPreventivi(List<Preventivo> preventivi) {
		this.preventivi = preventivi;
	}
	
}
