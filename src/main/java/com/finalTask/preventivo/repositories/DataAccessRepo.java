package com.finalTask.preventivo.repositories;

import java.util.List;

import org.springframework.stereotype.Repository;
@Repository
public interface DataAccessRepo <T>{

	public T save(T t);
	public T findById(int id);
	public List<T> findAll();
	public boolean delete(int id);
	public boolean update(T t);
}
