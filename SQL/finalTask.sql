drop DATABASE IF EXISTS finalTask;
create DATABASE finalTask;
use finalTask;


create table indirizzo(
	id_indirizzo int PRIMARY key AUTO_INCREMENT,
    via varchar(255),
    città varchar(100),
    cap int,
    provincia varchar(2)
);

create table azienda(
	id_azienda int primary key AUTO_INCREMENT,
    nome_azienda varchar(255),
    rif_indirizzo int,
    FOREIGN KEY (rif_indirizzo) REFERENCES indirizzo (id_indirizzo) on delete cascade
);

create table categoria(
	id_categoria int primary key AUTO_INCREMENT,
    nome varchar(30)
);

create table prodotto(
	id_prodotto int primary key AUTO_INCREMENT,
    codice varchar(10),
    nome varchar(30),
    quantità int,
    prezzo float
);

create table prodotto_categoria(
	rif_prodotto int,
    rif_categoria int,
    FOREIGN KEY (rif_prodotto) REFERENCES prodotto(id_prodotto),
    FOREIGN KEY (rif_categoria) REFERENCES categoria(id_categoria)
);

create table preventivo(
	id_preventivo int primary key AUTO_INCREMENT,
    numero_preventivo varchar(10),
    prezzo_totale float,
    voci text,
    data_preventivo DATE default now()
);

create table preventivo_azienda_prodotto(
	rif_preventivo int,
    rif_prodotto int,
    rif_azienda int,
    FOREIGN KEY (rif_preventivo) REFERENCES preventivo(id_preventivo),
    FOREIGN KEY (rif_prodotto) REFERENCES prodotto(id_prodotto),
    FOREIGN KEY (rif_azienda) REFERENCES azienda(id_azienda)
);

insert into indirizzo(via, città, cap, provincia) VALUE
("via mia azienda","città mia azienda",89024,"RC");
insert into azienda(nome_azienda, rif_indirizzo) VALUE
("azienda fallita",1);


select * from indirizzo;
select * from azienda;